
var numero=window.prompt("Escribe un numero");//Cogemos el numero
var descomposicion=[];
for(var n=parseInt(numero), i=2;n>1;){
    if(n%i==0){//Si es divisible entre un numero nos guardamos el numero y dividimos el numero original
        descomposicion[descomposicion.length]=i;
        n/=i;
    }else{//Si no es divisible entre ese numero pasamos al siguiente
        i++;
    }
}

if(descomposicion.length==1&&descomposicion[0]==parseInt(numero)){//Si el tamaño es uno ya significaria que es primo pero por si acaso miramos que el numero entre el que ha sido dividido es el mismo
    window.alert("El numero "+numero+" es primo");
}else{//Si no es primo simplemente mostramos el array con los numeros de la descomposicion
    window.alert("La descomposicion del numero "+numero+" es:\n"+descomposicion.toString());
}