var mech_counter = 0;
var selectedPlayer;
var teamRotation = 0;
var mode = 0;
var players = [];
var map = document.getElementById("map");
var team0 = document.getElementById("team0");
var team1 = document.getElementById("team1");
var ongoingAnimation = false;

function dist(x1, x2, y1, y2) {
    return Math.sqrt((x2 -= x1) * x2 + (y2 -= y1) * y2);
}

class Weapon {
    constructor(id) {
        switch (id) {
            case 0:
                this.name = "Energy Weapon";
                this.range = 10;
                this.damage = 4;
                this.nShots = 1;
                this.shotColor = "#05E7FF"
                break;
            case 1:
                this.name = "Canon";
                this.range = 7;
                this.damage = 6;
                this.nShots = 1;
                this.shotColor = "#FFF005"
                break;
            case 2:
                this.name = "Auto Canon";
                this.range = 7;
                this.damage = 4;
                this.nShots = 8;
                this.shotColor = "#FF9B05"
                break;
            case 3:
                this.name = "Missile Launch";
                this.range = 7;
                this.damage = 3;
                this.nShots = 3;
                this.shotColor = "#FF0505"
                break;

        }
    }
}

class Player {
    constructor(pilot_name = "Pilot " + mech_counter, mech_name = "Mech " + mech_counter, weapon1_id, weapon2_id, tileSize, posx = 0, posy = 0, rotation = 0, team = 0) {
        this.hp = 50;
        this.maxhp = 50;
        this.id = mech_counter++;
        this.pos_x = posx;
        this.pos_y = posy;
        this.team = team;
        this.tileSize = tileSize;
        this.mov = 5;
        this.pilot_name = pilot_name;
        this.mech_name = mech_name;
        this.weapon1 = new Weapon(weapon1_id);
        this.weapon2 = new Weapon(weapon2_id);
        this.moving = false;
        this.rotation = rotation;
        this.actions = 2;

        this.createGraphics();
        this.createHud();

        let found = false;
        for (let i = 0; i < tiles.length && !found; i++) {
            var tile = tiles[i];
            if (tile.posx == posx && tile.posy == posy) {
                this.tile = tile;
                found = true;
            }
        }
        if (players[team] == null) {
            players[team] = [this];
        } else {
            players[team][players[team].length] = this;
        }

    }

    createGraphics() {
        this.div = document.createElement("div");
        this.div.classList.add("player");
        this.div.style.width = this.tileSize + "px";
        this.div.style.height = this.tileSize + "px";
        this.div.style.left = (this.tileSize * this.pos_x) + "px";
        this.div.style.top = (this.tileSize * this.pos_y) + "px";
        this.img = document.createElement("IMG");
        this.img.setAttribute("src", "imgs/team" + this.team + ".svg");
        this.img.classList.add("test");
        const esto = this;
        this.img.addEventListener("click", function () {
            if (esto.team == selectedPlayer.team && esto.actions > 0) {
                mode = 0;
                selectedPlayer = esto
                resetTiles();
            }

            if (esto != selectedPlayer && esto.team != selectedPlayer.team && mode == 2 && esto.tile.isSelected) {
                selectedPlayer.shotMech(esto);
            }
        });
        this.div.appendChild(this.img);
        this.div.style.transform = "rotate(" + this.rotation + "deg)";
        map.appendChild(this.div);
    }

    createHud() {
        var hud = document.createElement("div");
        hud.classList.add("hud" + this.team);
        var border = this.team == 0 ? "border-dark" : "border-white";
        var table = this.team == 0 ? "table" : "table table-dark";
        hud.innerHTML =
            '<h4>' + this.pilot_name + '</h4>' +
            '<h6>' + this.mech_name + '</h6>' +
            '<div class="progress border ' + border + '">' +
            '<div class="progress-bar border  ' + border + '  bg-success" id="hp' + this.id + '" role="progressbar" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '<div class="progress border  ' + border + '" style="margin-top: 5px;">' +
            '<div class="progress-bar border  ' + border + ' bg-warning" id="action' + this.id + '-0" role="progressbar" style="width: 50%">' +
            '</div>' +
            '<div class="progress-bar border  ' + border + ' bg-warning" id="action' + this.id + '-1" role="progressbar" style="width: 50%">' +
            '</div>' +
            '</div>' +
            '<table class="' + table + '">' +
            '<thead>' +
            '<tr>' +
            '<th scope="col">Name</th>' +
            '<th scope="col">Damage</th>' +
            '<th scope="col">Range</th>' +
            '<th scope="col">nShoots</th>' +
            '</tr>' +
            '</thead>' +
            '<tbody>' +
            '<tr>' +
            '<th scope="row" style="color: ' + this.weapon1.shotColor + '>' + this.weapon1.name + '</th>' +
            '<td>' + this.weapon1.damage + '</td>' +
            '<td>' + this.weapon1.range + '</td>' +
            '<td>' + this.weapon1.nShots + '</td>' +
            '</tr>' +
            '<tr>' +
            '<th scope="row" style="color: ' + this.weapon2.shotColor + ';">' + this.weapon2.name + '</th>' +
            '<td>' + this.weapon2.damage + '</td>' +
            '<td>' + this.weapon2.range + '</td>' +
            '<td>' + this.weapon2.nShots + '</td>' +
            '</tr>' +
            '</tbody>' +
            '</table>';

        this.hud = hud;

        if (this.team == 0) {
            team0.appendChild(this.hud);
        } else {
            team1.appendChild(this.hud);
        }

        this.hpBar = document.getElementById("hp" + this.id);
        this.actionBar = [];
        this.actionBar[0] = document.getElementById("action" + this.id + "-0");
        this.actionBar[1] = document.getElementById("action" + this.id + "-1");

    }

    shotMech(player) {
        var delta_x = player.pos_x - this.pos_x;
        var delta_y = player.pos_y - this.pos_y;
        var rotation = Math.atan2(delta_y, delta_x) * (180 / Math.PI);
        const esto = this;
        resetTiles()
        this.div.animate([{}, { "transform": "rotate(" + rotation + "deg)" }], { "duration": (this.rotation != rotation) * 500, "fill": "forwards", "easing": "ease-in-out" }).onfinish = function () {
            var distancia = dist(esto.pos_x, player.pos_x, esto.pos_y, player.pos_y)
            if (esto.weapon1.range >= distancia) {
                var bullet = document.createElement("img");
                bullet.setAttribute("src", "imgs/bullet.svg");
                bullet.style.position = "absolute";
                bullet.style.width = esto.tileSize + "px";
                bullet.style.height = esto.tileSize + "px";
                bullet.style.left = (esto.tileSize * esto.pos_x) + "px";
                bullet.style.top = (esto.tileSize * esto.pos_y) + "px";
                map.appendChild(bullet);
                bullet.animate([{}, { "transform": "rotate(" + rotation + "deg)" }], { "duration": 0, "fill": "forwards" }).onfinish = function () {
                    bullet.animate([{}, { "left": (player.pos_x * esto.tileSize) + "px", "top": (player.pos_y * esto.tileSize) + "px" }], { "duration": distancia * 150, "fill": "forwards", "easing": "ease-in" }).onfinish = function () {
                        player.damage(esto.weapon1.damage);
                        map.removeChild(bullet);
                    }
                }

            }
            /*
            if (esto.weapon2.range >= distancia) {
                player.damage(esto.weapon2.damage);
            }
            */
            esto.setActions(0);
            nextMember();
        };

    }

    damage(damage) {
        this.hp -= damage;
        this.hpBar.animate([{}, { "width": Math.floor((this.hp / this.maxhp) * 100) + "%" }], { "duration": damage * 100, "fill": "forwards" });
        if (this.hp < 0) {
            const esto = this;
            this.div.animate([{}, { "width": "0%", "height": "0%" }], { "duration": 2000, "fill": "forwards" }).onfinish = function () {
                map.removeChild(esto.div);
                var array = players[esto.team];
                const index = array.indexOf(esto);
                console.log(index);
                if (index > -1) {
                    array.splice(index, 1);
                }
                if (esto.team == 0) {
                    team0.removeChild(esto.hud);
                } else {
                    team1.removeChild(esto.hud);
                }
            };
        }

    }

    setActions(val) {
        this.actions = val;
        switch (this.actions) {
            case 0:
                this.actionBar[0].animate([{}, { "width": "0%" }], { "duration": 1000, "fill": "forwards" });
                this.actionBar[1].animate([{}, { "width": "0%" }], { "duration": 1000, "fill": "forwards" });
                break
            case 1:
                this.actionBar[0].animate([{}, { "width": "50%" }], { "duration": 1000, "fill": "forwards" });
                this.actionBar[1].animate([{}, { "width": "0%" }], { "duration": 1000, "fill": "forwards" });
                break
            case 2:
                this.actionBar[0].animate([{}, { "width": "50%" }], { "duration": 1000, "fill": "forwards" });
                this.actionBar[1].animate([{}, { "width": "50%" }], { "duration": 1000, "fill": "forwards" });
                break
        }
    }

    moveTo(posx, posy) {

        var hEnd = { "top": (posy * this.tileSize) + "px" };
        var hMov = [{}, hEnd];
        var hOptions = { "duration": Math.abs(this.pos_y - posy) * 250, "fill": "forwards", "easing": "ease-in-out" };
        var hOrientation = (this.pos_y - posy) / Math.abs(this.pos_y - posy);
        var vOrientation = (this.pos_x - posx) / Math.abs(this.pos_x - posx);
        var vEnd = { "left": (posx * this.tileSize) + "px" };
        var vMov = [{}, vEnd];
        var vOptions = { "duration": Math.abs(this.pos_x - posx) * 250, "fill": "forwards", "easing": "ease-in-out" };

        if (this.pos_x - posx != 0 || this.pos_y - posy != 0) {

            this.moving = true;
            resetTiles();
            var movement = Math.abs(this.pos_x - posx) > Math.abs(this.pos_y - posy) && this.pos_y - posy != 0 || this.pos_x - posx == 0 ? "y" : "x";
            const esto = this;
            var rotation = (movement == "x" ? (vOrientation == 1) * 180 : hOrientation * -1 * 90);
            this.div.animate([{}, { "transform": "rotate(" + rotation + "deg)" }], { "duration": (this.rotation != rotation) * 500, "fill": "forwards", "easing": "ease-in-out" }).onfinish = function () {
                esto.rotation = rotation;
                esto.div.animate(movement == "x" ? vMov : hMov, movement == "x" ? vOptions : hOptions).onfinish = function () {

                    if (movement == "x") {
                        esto.pos_x = posx;
                    } else {
                        esto.pos_y = posy;
                    }
                    esto.moving = false;

                    if (esto.pos_x - posx != 0 || esto.pos_y - posy != 0) {
                        esto.moveTo(posx, posy);
                    } else if (esto.actions > 0) {
                        movementMode();
                    } else {
                        nextMember();
                    }
                };
            };

        }
    }




};

class Tile {
    constructor(posx, posy, size) {
        this.posx = posx;
        this.posy = posy;
        this.div = document.createElement("div");
        this.div.classList.add("tile");
        this.div.style.width = size + "px";
        this.div.style.height = size + "px";
        this.div.style.left = (size * posx) + "px";
        this.div.style.top = (size * posy) + "px";
        this.baseColor = "darkgreen";
        this.bgcolor = "darkgreen";
        this.isSelected = false;
        this.movCost = 0;
        const esto = this;
        this.div.addEventListener("mouseenter", function (e) {
            e.target.style.backgroundColor = "darkblue";
        });
        this.div.addEventListener("mouseleave", function (e) {
            e.target.style.backgroundColor = esto.bgcolor;
        });
        this.div.addEventListener("click", function () {
            if (mode == 1 && esto.isSelected && selectedPlayer != null && !selectedPlayer.moving) {
                selectedPlayer.setActions(selectedPlayer.actions - esto.movCost);
                resetTiles();
                selectedPlayer.moveTo(posx, posy);
                selectedPlayer.tile = esto;
            }
        })
    }

    changeTileColor(color) {
        console.log(color);
        this.bgcolor = color;
        this.div.style.backgroundColor = color;
    }

    setMovCost(cost) {
        this.movCost = cost;
    }

    resetTile() {
        this.bgcolor = this.baseColor;
        this.div.style.backgroundColor = tile.bgcolor;
        this.isSelected = false;
        this.movCost = 0;
    }
};
var map;
var tiles = [];
var tileSize = 50;
var colNum = 25;
var rowNum = 13;

var row = document.getElementById("row1");

row.setAttribute("height", (tileSize * rowNum) + "px");
row.style.height = (tileSize * rowNum) + "px";

for (var i = 0; i < colNum; i++) {
    for (var j = 0; j < rowNum; j++) {
        var tile = new Tile(i, j, tileSize);
        tiles[tiles.length] = tile;
        map.appendChild(tile.div);
    }
}

function resetTiles() {
    tiles.forEach(function (tile) {
        tile.resetTile();
    })
    if (!selectedPlayer.moving) {
        selectedPlayer.tile.changeTileColor("darkblue");
    }
}

function movementMode() {
    resetTiles();
    mode = 1;
    tiles.forEach(function (tile) {
        var distance = Math.abs(selectedPlayer.pos_x - tile.posx) + Math.abs(selectedPlayer.pos_y - tile.posy);
        if (distance <= selectedPlayer.mov) {
            tile.isSelected = true;
            tile.changeTileColor(selectedPlayer.actions < 2 ? "lightsalmon" : "lightskyblue");
            tile.setMovCost(1);
        } else if (distance <= selectedPlayer.mov * 2 && selectedPlayer.actions > 1) {
            tile.isSelected = true;
            tile.changeTileColor("lightsalmon");
            tile.setMovCost(2);
        }
    })
}

function shootMode() {
    resetTiles();
    mode = 2;
    tiles.forEach(function (tile) {
        var distance = Math.abs(selectedPlayer.pos_x - tile.posx) + Math.abs(selectedPlayer.pos_y - tile.posy);
        if (distance <= selectedPlayer.weapon1.range && distance <= selectedPlayer.weapon2.range) {
            tile.isSelected = true;
            tile.changeTileColor("lightcoral");
        } else if (distance <= selectedPlayer.weapon1.range || distance <= selectedPlayer.weapon2.range) {
            tile.isSelected = true;
            tile.changeTileColor("lightsalmon");
        }
    });
    for (var i = 0; i < players.length; i++) {
        if (i == selectedPlayer.team) {
            continue;
        }

        var enemies = players[i];
        enemies.forEach(function (enemy) {
            var distance = Math.abs(selectedPlayer.pos_x - enemy.pos_x) + Math.abs(selectedPlayer.pos_y - enemy.pos_y);
            if (distance <= selectedPlayer.weapon1.range || distance <= selectedPlayer.weapon2.range) {
                enemy.tile.changeTileColor("red");
            }
        })
    }
}

function nextMember() {
    var team = players[teamRotation];
    var nextPlayer;
    for (let i = 0; i < team.length; i++) {
        if (team[i].actions > 0 && team[i].hp > 0) {
            nextPlayer = team[i];
        }
    }
    if (nextPlayer != null) {
        selectedPlayer = nextPlayer;
        resetTiles();
    } else {
        switchTeam();
    }
}

function switchTeam() {
    teamRotation++;
    teamRotation = teamRotation % players.length;
    players[teamRotation].forEach(function (player) {
        player.setActions(2);
    })
    nextMember();
}

var player1 = new Player("Manolo", "Roboto", 1, 1, tileSize, 3, 8, 50, 0);
var player12 = new Player("Manolo", "Roboto", 1, 1, tileSize, 5, 5, 0, 0);
var player2 = new Player("Paco", "Cacharro", 1, 1, tileSize, 3, 10, -70, 1);


nextMember();







